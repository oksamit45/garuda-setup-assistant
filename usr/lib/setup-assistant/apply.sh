echo ""
echo "Installing packages.."
echo ""
sudo pacman -Sy
installable_packages=$(comm -12 <(pacman -Slq | sort) <(sed s/\\s/\\n/g - <$1 | sort))
sudo pacman -Su --needed $installable_packages

if [ -e "$2" ]; then
	echo ""
	echo "Enabling services.."
	echo ""
	sudo bash - <$2
fi

echo ""
read -p "Press enter to finish"
